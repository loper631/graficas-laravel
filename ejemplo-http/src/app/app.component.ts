import { Component, OnInit } from '@angular/core';
import { GirosService } from './serices/giros.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'ejemplo-http';
  data:any = [];
  primera:any;
  single: any[]=[{
    "name": "Finalizados",
    "value": 1000
  },
  {
    "name": "Ingresado",
    "value": 2000
  },
  { 
    "name": "France",
    "value": 3000
  }];

  view: any[]= [700,400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Country';
  showYAxisLabel = true;
  yAxisLabel = 'Population';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  constructor(private _giroService:GirosService){
    Object.assign(this.single)
  }
  
  onSelect(event:any) {
    console.log(event);
  }

  ngOnInit(): void {
    this._giroService.getGiros().subscribe((respuesta:any)=>{
      console.log(respuesta);
      this.data = respuesta.data;
      this.primera = respuesta.data[0];
    },e=>{
      console.error(e);
    });
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    
  }



}
