import { TestBed } from '@angular/core/testing';

import { GirosService } from './giros.service';

describe('GirosService', () => {
  let service: GirosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GirosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
