import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class GirosService {

  constructor(private _http:HttpClient) { }

  getGiros(){
    return this._http.get('https://api-visorurbano.jalisco.gob.mx/giros_public/getEncendidos?municipio_id=1');
  }

}
